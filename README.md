# mathlib

Mathlib is yet another simple mathematic library for C++ language.



## Features

- Vectors
- Matrices
- Translate/scale/rotate matrices
- Factorial
- Binomial coeficient
- Sigmoid function
- Radians <-> Degrees conversion
