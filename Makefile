NAME = mathlib.a
AR = ar
ARFLAGS = rc
CPPC = g++
CPPFLAGS = -Wall -Wextra -Werror -Wno-unused-result -std=c++17 -I src/ -O3

SRC_DIR = src/
SRC := $(shell find $(SRC_DIR) -type f -name "*.cpp")
HDR := $(shell find $(SRC_DIR) -type f -name "*.hpp")
DIRS := $(shell find $(SRC_DIR) -type d)
OBJ_DIR = obj/
OBJ := $(patsubst $(SRC_DIR)%.cpp,$(OBJ_DIR)%.o,$(SRC))
OBJ_DIRS := $(patsubst $(SRC_DIR)%,$(OBJ_DIR)%,$(DIRS))

all: $(NAME) tags

$(OBJ_DIRS):
	mkdir -p $(OBJ_DIRS)

$(NAME): $(OBJ_DIRS) $(OBJ)
	$(AR) $(ARFLAGS) $@ $(OBJ)

$(OBJ_DIR)%.o: $(SRC_DIR)%.cpp $(HDR)
	$(CPPC) $(CPPFLAGS) -o $@ -c $<

tags: $(SRC) $(HDR)
	ctags $(SRC) $(HDR)

clean:
	rm -rf $(OBJ_DIR)

fclean: clean
	rm -f $(NAME)
	rm -f tags

re: fclean all

.PHONY: all clean fclean re
