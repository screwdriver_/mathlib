#include <math.hpp>

using namespace math;

void math::print_float_binary(const float v)
{
	union { float f; uint32_t i; } val;
	size_t i;

	val.f = v;
	for(i = sizeof(val) * 8; i > 0; --i)
		std::cout << ((val.i >> (i - 1)) & 1);
}
