#ifndef LINEAR_ALGEBRA_HPP
# define LINEAR_ALGEBRA_HPP

# include <complex>
# include <cstring>
# include <functional>
# include <initializer_list>

namespace math
{
	template<typename Type, bool Transpose = false>
	class mat;

	template<typename Type>
	class vec;

	template<typename Type>
	class mat_storage
	{
		public:
		inline mat_storage(const size_t height, const size_t width)
			: h{height}, w{width}
		{
			if(width == 0 || height == 0)
				throw std::invalid_argument("Null matrix");
			if(!(data = new Type[height * width]))
				throw std::runtime_error("Allocation failed");
			bzero(data, height * width * sizeof(Type));
		}

		inline mat_storage(const size_t height, const size_t width, const std::initializer_list<Type> &list)
			: mat_storage(height, width)
		{
			std::copy(list.begin(), list.begin() + (height * width), data);
		}

		inline mat_storage(const mat_storage &s)
			: h{s.h}, w{s.w}
		{
			if(!(data = new Type[h * w]))
				throw std::runtime_error("Allocation failed");
			memcpy(data, s.data, s.h * s.w * sizeof(Type));
		}

		inline ~mat_storage()
		{
			delete[] data;
		}

		inline mat_storage &operator=(const mat_storage &s)
		{
			h = s.h;
			w = s.w;
			delete[] data;
			if(!(data = new Type[h * w]))
				throw std::runtime_error("Allocation failed");
			memcpy(data, s.data, s.h * s.w * sizeof(Type));
			return *this;
		}

		inline size_t height() const
		{
			return h;
		}

		inline size_t width() const
		{
			return w;
		}

		inline operator Type *()
		{
			return data;
		}

		inline operator const Type *() const
		{
			return data;
		}

		inline Type &operator[](const size_t i)
		{
			return data[i];
		}

		inline const Type &operator[](const size_t i) const
		{
			return data[i];
		}

		private:
		size_t h, w;
		Type *data;
	};

	template<typename Mat, typename Type>
	class mat_line
	{
		public:
		inline mat_line(Mat &m, const size_t index)
			: m{m}, index{index}
		{}

		inline operator Type()
		{
			return operator[](0);
		}

		inline operator const Type() const
		{
			return operator[](0);
		}

		inline void operator=(const Type val)
		{
			operator[](0) = val;
		}

		inline Type &operator[](const size_t j)
		{
			return m(index, j);
		}

		inline const Type &operator[](const size_t j) const
		{
			return m(index, j);
		}

		private:
		std::reference_wrapper<Mat> m;
		size_t index;
	};

	// TODO Check argument matrix size before operation
	template<typename Type, bool Transpose>
	class mat
	{
		using mat_ = mat<Type, Transpose>;
		using const_mat_ = const mat<const Type, Transpose>;
		using line_ = mat_line<mat_, Type>;
		using const_line_ = mat_line<const_mat_, const Type>;

		public:
		using transpose = mat<Type, !Transpose>;

		inline mat(const size_t height, const size_t width)
			: data(height, width)
		{}

		inline mat(const size_t height, const size_t width, const std::initializer_list<Type> &list)
			: data(height, width, list)
		{}

		inline size_t height() const
		{
			return Transpose ? data.width() : data.height();
		}

		inline size_t width() const
		{
			return Transpose ? data.height() : data.width();
		}

		inline constexpr bool is_square() const
		{
			return (width() == height());
		}

		inline void operator=(const std::initializer_list<Type> &list)
		{
			std::copy(list.begin(), list.end(), data);
		}

		inline operator Type *()
		{
			return data;
		}

		inline operator const Type *() const
		{
			return data;
		}

		inline line_ operator[](const size_t i)
		{
			if(i >= height())
				throw std::invalid_argument("Bad index");
			return line_(*this, i);
		}

		inline const_line_ operator[](const size_t i) const
		{
			if(i >= height())
				throw std::invalid_argument("Bad index");
			return const_line_(*reinterpret_cast<const_mat_ *>(this), i);
		}

		inline Type &operator()(const size_t i, const size_t j)
		{
			if(i >= height() || j >= width())
				throw std::invalid_argument("Bad index");
			if(Transpose)
				return data[j * height() + i];
			return data[i * width() + j];
		}

		inline const Type &operator()(const size_t i, const size_t j) const
		{
			if(i >= height() || j >= width())
				throw std::invalid_argument("Bad index");
			if(Transpose)
				return data[j * height() + i];
			return data[i * width() + j];
		}

		mat<Type> submatrix(const size_t i, const size_t j, const size_t height, const size_t width)
		{
			mat<Type> m(height, width);
			size_t y, x;

			for(y = 0; y < height; ++y)
				for(x = 0; x < width; ++x)
					m(y, x) = operator()(i + y, j + x);
			return m;
		}

		mat<Type> operator+(const mat_ &m) const
		{
			mat<Type> r(height(), width());
			size_t i, j;

			for(i = 0; i < height(); ++i)
				for(j = 0; j < width(); ++j)
					r(i, j) = operator()(i, j) + m(i, j);
			return r;
		}

		void operator+=(const mat_ &m)
		{
			size_t i, j;

			for(i = 0; i < height(); ++i)
				for(j = 0; j < width(); ++j)
					operator()(i, j) += m(i, j);
		}

		mat<Type> operator-() const
		{
			mat<Type> r(height(), width());
			size_t i, j;

			for(i = 0; i < height(); ++i)
				for(j = 0; j < width(); ++j)
					r(i, j) = -operator()(i, j);
			return r;
		}

		mat<Type> operator-(const mat_ &m) const
		{
			mat<Type> r(height(), width());
			size_t i, j;

			for(i = 0; i < height(); ++i)
				for(j = 0; j < width(); ++j)
					r(i, j) = operator()(i, j) - m(i, j);
			return r;
		}

		void operator-=(const mat_ &m)
		{
			size_t i, j;

			for(i = 0; i < height(); ++i)
				for(j = 0; j < width(); ++j)
					operator()(i, j) -= m(i, j);
		}

		mat<Type> operator*(const Type val) const
		{
			mat<Type> r(height(), width());
			size_t i, j;

			for(i = 0; i < height(); ++i)
				for(j = 0; j < width(); ++j)
					r(i, j) = operator()(i, j) * val;
			return r;
		}

		void operator*=(const Type val)
		{
			size_t i, j;

			for(i = 0; i < height(); ++i)
				for(j = 0; j < width(); ++j)
					operator()(i, j) *= val;
		}

		// TODO Optimization
		template<bool T>
		mat<Type> operator*(const mat<Type, T> &m) const
		{
			if(m.height() != width())
				throw std::invalid_argument("Bad matrix multiplication");
			mat<Type> r(height(), m.width());
			size_t i, j, k;
			Type sum;

			for(i = 0; i < height(); ++i)
				for(j = 0; j < m.width(); ++j)
				{
					sum = 0;
					for(k = 0; k < width(); ++k)
						sum = std::fma(operator()(i, k), m(k, j), sum);
					r(i, j) = sum;
				}
			return r;
		}

		// TODO Optimization
		template<bool T>
		void operator*=(const mat<Type, T> &m)
		{
			if(m.height() != height() || m.width() != width())
				throw std::invalid_argument("Bad matrix multiplication");
			size_t i, j, k;
			Type sum;

			for(i = 0; i < height(); ++i)
				for(j = 0; j < m.width(); ++j)
				{
					sum = 0;
					for(k = 0; k < width(); ++k)
						sum = std::fma(operator()(i, k), m(k, j), sum);
					operator()(i, j) = sum;
				}
		}

		void hadamard_product(const mat_ &m)
		{
			// TODO Check that matrixes are same size
			size_t i, j;

			for(i = 0; i < height(); ++i)
				for(j = 0; j < width(); ++j)
					operator()(i, j) *= m(i, j);
		}

		mat<Type> kronecker_product(const mat_ &m) const
		{
			mat<Type> r(height() * m.height(), width() * m.width());
			size_t i, j, k, l;

			for(i = 0; i < height(); ++i)
				for(j = 0; j < width(); ++j)
				{
					const auto &v(operator()(i, j));
					if(!v)
						continue;
					for(k = 0; k < m.height(); ++k)
						for(l = 0; l < m.width(); ++l)
							r(i * m.height() + k, j * m.width() + l) = v * m(k, l);
				}
			return r;
		}

		inline mat::transpose& transpose_matrix()
		{
			return *reinterpret_cast<mat::transpose *>(this);
		}

		inline const mat::transpose& transpose_matrix() const
		{
			return *reinterpret_cast<const mat::transpose *>(this);
		}

		void row_swap(const size_t r0, const size_t r1)
		{
			// TODO Check if r0 or r1 is >= height()
			if(r0 == r1)
				return;
			size_t i;

			for(i = 0; i < width(); ++i)
				std::swap(operator()(r0, i), operator()(r1, i));
		}

		void row_foreach(const size_t r, const std::function<void(Type &)> f)
		{
			// TODO Check if r >= height()
			size_t i;

			for(i = 0; i < width(); ++i)
				f(operator()(r, i));
		}

		void row_foreach(const size_t r0, const size_t r1, const std::function<void(Type &, const Type &)> f)
		{
			// TODO Check if r0 or r1 is >= height()
			size_t i;

			for(i = 0; i < width(); ++i)
				f(operator()(r0, i), operator()(r1, i));
		}

		void gauss_elimination()
		{
			size_t i, j, r(0);

			for(i = 0; i < width() && r < height(); ++i)
			{
				const auto max(get_max_row(r, i));
				if(operator()(max, i) == 0)
					continue;
				const auto &tmp0(operator()(max, i));
				row_foreach(max, [this, tmp0](Type &val)
					{
						val /= tmp0;
					});
				row_swap(max, r);
				for(j = 0; j < height(); ++j)
					if(j != r)
					{
						const auto &tmp1(operator()(j, i));
						row_foreach(j, r, [this, tmp1](Type &v0, const Type &v1)
							{
								v0 = std::fma(tmp1, v1, -v0);
							});
					}
				++r;
			}
		}

		inline bool is_inversible() const
		{
			return EPSILON_CMP(determinant(), 0.);
		}

		// TODO Optimize
		mat<Type> inverse_matrix() const
		{
			mat<Type> m(height(), width() * 2);
			size_t i, j;

			for(i = 0; i < height(); ++i)
				for(j = 0; j < width(); ++j)
					m(i, j) = operator()(i, j);
			for(i = 0; i < height(); ++i)
				m(i, width() + i) = 1;
			m.gauss_elimination();
			return m.submatrix(0, width(), height(), width());
		}

		// TODO Optimize
		// TODO Check result
		Type determinant() const
		{
			if(height() != width())
				throw std::invalid_argument("Matrix must be a square");
			if(width() == 1)
				return data(0, 0);
			if(width() == 2)
				return data(0, 0) * data(1, 1) - data(1, 0) * data(0, 1);

			mat_ m(*this);
			m.gauss_elimination();

			Type r(m.data[0]);
			for(size_t i = 1; i < m.width(); ++i)
				r *= m(i, i);
			return r;
		}

		Type matrix_minor(const size_t row, const size_t col)
		{
			// TODO
			return 0;
		}

		inline Type cofactor(const size_t row, const size_t col)
		{
			const auto m(matrix_minor(row, col));
			return ((col + row) & 1 ? -m : m);
		}

		Type trace() const
		{
			if(height() != width())
				throw std::invalid_argument("Matrix must be a square");
			Type r(0);
			size_t i;

			for(i = 0; i < width(); ++i)
				r += operator()(i, i);
			return r;
		}

		vec<Type> to_vec() const
		{
			return vec<Type>(*this);
		}

		protected:
		mat_storage<Type> data;

		private:
		size_t get_max_row(const size_t start_row, const size_t col) const
		{
			size_t i, max_index;
			Type tmp, max;

			max = std::abs(operator()(start_row, col));
			max_index = start_row;
			for(i = start_row + 1; i < height(); ++i)
			{
				tmp = std::abs(operator()(i, col));
				if(tmp > max)
				{
					max = tmp;
					max_index = i;
				}
			}
			return max_index;
		}
	};

	template<typename Type>
	class vec : public mat<Type, false>
	{
		using vec_ = vec<Type>;
		using parent = mat<Type, false>;

		public:
		inline vec(const size_t size)
			: parent(size, 1)
		{}

		inline vec(const size_t size, const std::initializer_list<Type> &list)
			: parent(size, 1, list)
		{}

		inline vec(const std::vector<Type> &v)
			: parent(v.size(), 1)
		{
			for(size_t i(0); i < v.size(); ++i)
				operator[](i) = v[i];
		}

		template<bool Tr>
		inline vec(const mat<Type, Tr> &m)
			: parent(m.height(), 1)
		{
			size_t i;

			for(i = 0; i < m.height(); ++i)
				parent::operator()(i, 0) = m(i, 0);
		}

		inline vec(const std::complex<Type> &c)
			: parent(2, 1, {c.real, c.comp})
		{}

		constexpr size_t size() const
		{
			return parent::height();
		}

		inline void operator=(const std::complex<Type> &c)
		{
			if(size() != 2)
				throw std::runtime_error("Invalid size for complex vector");
			parent::data[0] = c.real;
			parent::data[1] = c.comp;
		}

		inline Type &operator[](const size_t i)
		{
			return parent::operator()(i, 0);
		}

		inline const Type &operator[](const size_t i) const
		{
			return parent::operator()(i, 0);
		}

		inline Type &operator()(const size_t i)
		{
			return parent::operator()(i, 0);
		}

		inline const Type &operator()(const size_t i) const
		{
			return parent::operator()(i, 0);
		}

		inline Type dot(const vec_ &v) const
		{
			if(size() != v.size())
				throw std::runtime_error("Vector must of the same size for dot product");
			Type r(0);
			size_t i;

			for(i = 0; i < v.size(); ++i)
				r = std::fma(operator[](i), v[i], r);
			return r;
		}

		Type length2() const
		{
			Type r(0), tmp;
			size_t i;

			for(i = 0; i < size(); ++i)
			{
				tmp = operator[](i);
				r = std::fma(tmp, tmp, r);
			}
			return r;
		}

		Type length() const
		{
			return sqrt(length2());
		}

		vec_ normalize(const Type val)
		{
			Type l;
			size_t i;

			l = length();
			for(i = 0; i < size(); ++i)
				operator[](i) = operator[](i) * val / l;
			return *this;
		}

		inline operator std::vector<Type>()
		{
			std::vector<Type> v(size());

			for(size_t i(0); i < size(); ++i)
				v[i] = operator[](i);
			return v;
		}

		inline operator std::complex<Type>()
		{
			if(size() != 2)
				throw std::runtime_error("Vector must be of size 2 for conversion to complex");
			return {parent::operator[](0), parent::operator[](1)};
		}
	};

	template<typename Type>
	inline std::ostream &operator<<(std::ostream &o, const vec<Type> &v)
	{
		size_t i;

		o << "[ ";
		for(i = 0; i < v.size(); ++i)
			o << v[i] << ' ';
		o << ']';
		return o;
	}

	template<typename Type, bool Transpose>
	inline std::ostream &operator<<(std::ostream &o, const mat<Type, Transpose> &m)
	{
		size_t i, j;

		for(i = 0; i < m.height(); ++i)
		{
			o << "[ ";
			for(j = 0; j < m.width(); ++j)
				o << m(i, j) << ' ';
			o << "]\n";
		}
		return o;
	}

	vec<float> cross_product(const vec<float> &v0, const vec<float> &v1);

	mat<float> translate(const vec<float> &vec);
	mat<float> scale(const vec<float> &vec);

	mat<float> rotate_x(float angle);
	mat<float> rotate_y(float angle);
	mat<float> rotate_z(float angle);

	template<typename Type>
	constexpr mat<Type> identity(const size_t size)
	{
		mat<Type> m(size);
		size_t i;

		for(i = 0; i < size; ++i)
			m(i, i) = 1;
		return m;
	}

	mat<float> ortho(float top, float bottom, float left, float right, float near, float far);
	mat<float> perspective(float aspect, float fov, float near, float far);

	template<typename T>
	class vec2 : public vec<T>
	{
		public:
		inline vec2()
			: vec<T>(2)
		{}
	};

	template<typename T>
	class vec3 : public vec<T>
	{
		public:
		inline vec3()
			: vec<T>(3)
		{}
	};

	template<typename T>
	class vec4 : public vec<T>
	{
		public:
		inline vec4()
			: vec<T>(4)
		{}
	};
}

#endif
