#include "linear_algebra.hpp"

using namespace math;

mat<float> math::ortho(const float top, const float bottom,
	const float left, const float right, const float near, const float far)
{
	return mat<float>(4, 4, {
		2.f / (right - left), 0.f, 0.f, -((right + left) / (right - left)),
		0.f, 2.f / (top - bottom), 0.f, -((top + bottom) / (top - bottom)),
		0.f, 0.f, -2.f / (far - near), -((far + near) / (far - near)),
		0.f, 0.f, 0.f, 1.f
	});
}

mat<float> math::perspective(const float aspect, const float fov,
	const float near, const float far)
{
	const float t = tan(fov / 2.f), z_range = near - far;
	return mat<float>(4, 4, {
		1.0f / (aspect * t), 0.f, 0.f, 0.f,
		0.f, 1.0f / t, 0.f, 0.f,
		0.f, 0.f, (-near - far) / z_range, (2 * far * near) / z_range,
		0.f, 0.f, 1.f, 0.f
	});
}
