#include "linear_algebra.hpp"

using namespace math;

vec<float> math::cross_product(const vec<float> &v0, const vec<float> &v1)
{
	return vec<float>(3, {
		v0[1] * v1[2] - v0[2] * v1[1],
		v0[2] * v1[0] - v0[0] * v1[2],
		v0[0] * v1[1] - v0[1] * v1[0]
	});
}
