#ifndef ALGEBRA_HPP
# define ALGEBRA_HPP

# include <cmath>

namespace math
{
	template<typename T>
	inline T poly_eval(size_t degree, const T *a, const T &x)
	{
		size_t i;
		T r(a[0]);

		for(i = 1; i < degree + 1; ++i)
			r = std::fma(r, x, a[i]);
		return r;
	}
}

#endif
