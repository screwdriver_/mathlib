#include "linear_algebra.hpp"

using namespace math;

mat<float> math::translate(const vec<float> &vec)
{
	return mat<float>(4, 4, {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		vec[0], vec[1], vec[2], 1
	});
}

mat<float> math::scale(const vec<float> &vec)
{
	return mat<float>(4, 4, {
		vec[0], 0, 0, 0,
		0, vec[1], 0, 0,
		0, 0, vec[2], 0,
		0, 0, 0, 1
	});
}
