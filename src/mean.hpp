#ifndef MEAN_HPP
# define MEAN_HPP

# include <cmath>

namespace math
{
	template<typename T>
	inline T arithmetic_mean(size_t n, const T *values)
	{
		size_t i;
		T r(0);

		for(i = 0; i < n; ++i)
			r += values[i];
		return r / n;
	}

	template<typename T>
	inline T geometric_mean(size_t n, const T *values)
	{
		size_t i;
		T r(0);

		for(i = 0; i < n; ++i)
			r += ln(values[i]);
		return exp(r / n);


	}

	template<typename T>
	inline T harmonic_mean(size_t n, const T *values)
	{
		size_t i;
		T r(0);

		for(i = 0; i < n; ++i)
			r += T(1) / values[i];
		return T(n) / r;

	}
}

#endif
