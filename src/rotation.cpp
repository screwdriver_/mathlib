#include "linear_algebra.hpp"

using namespace math;

mat<float> math::rotate_x(const float angle)
{
	const float s = sin(angle), c = cos(angle);
	return mat<float>(4, 4, {
		1, 0, 0, 0,
		0, c, -s, 0,
		0, s, c, 0,
		0, 0, 0, 1 
	});
}

mat<float> math::rotate_y(const float angle)
{
	const float s = sin(angle), c = cos(angle);
	return mat<float>(4, 4, {
		c, 0, s, 0,
		0, 1, 0, 0,
		-s, 0, c, 0,
		0, 0, 0, 1 
	});
}

mat<float> math::rotate_z(const float angle)
{
	const float s = sin(angle), c = cos(angle);
	return mat<float>(4, 4, {
		c, -s, 0, 0,
		s, c, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1 
	});
}
