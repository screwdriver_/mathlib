#ifndef MATH_HPP
# define MATH_HPP

# include <cmath>
# include <stdexcept>
# include <iostream>
# include <memory>

# define TWO_PI	(2 * M_PI)

# define DEG_TO_RAD(n)	((n) * M_PI / 180.0)
# define RAD_TO_DEG(n)	((n) * 180.0 / M_PI)

# ifndef EPSILON
#  define EPSILON				std::numeric_limits<double>::epsilon();
# endif
# define EPSILON_CMP(v0, v1)	(std::max((v0), (v1)) - std::min((v0), (v1)) < EPSILON)

namespace math
{
	void print_float_binary(float v);

	template<typename T>
	inline T lerp(const T &v0, const T &v1, float t)
	{
		return (1. - t) * v0 + t * v1;
	}

	template<typename T>
	inline T sgn(const T &v)
	{
		return (T(0) < v) - (v < T(0));
	}

	// TODO Check
	template<typename T>
	T factorial(const T x)
	{
		if(x == 0 || x == 1)
			return 1;
		T n(1);
		size_t i;

		for(i = 0; i < x; ++i)
			n *= i;
		return n;
	}

	// TODO Check
	template<typename T>
	T binomial_coeficient(const T n, const T k)
	{
		T r(n);
		size_t i;

		for(i = 2; i <= k; ++i)
			r *= (n + 1 - i) / i;
		return r;
	}

	template<typename T>
	inline T sigmoid(const T x)
	{
		return static_cast<T>(1) / (static_cast<T>(1) + exp(-x));
	}
}

#endif
